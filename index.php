<?php
require_once 'Classes/Session.php';
require_once 'Classes/Cookie.php';
require_once 'Classes/Auth.php';
require_once 'Classes/DatabaseLog.php';

Session::start();
echo Flash::getMessage('login');

if (!empty($_POST['exit'])) {
    Session::unsetVal('login');
    Session::sessionDestroy();
    Cookie::delete('authKey');
    header('Location: login.php');
}

if ($user = Auth::getUser(Cookie::get('authKey'))) {
    echo "<b>{$user['UserLogin']}</b> !!!";
} else {
    header('Location: login.php');
}
?>

<form action="index.php" method="post">
    <input type="hidden" name="exit" value="1">
    <input type="submit" value="Выйти">
</form>

