<?php
require_once 'Classes/Flash.php';
require_once 'Classes/Session.php';
require_once 'Classes/Auth.php';
require_once 'Classes/Cookie.php';
require_once 'Classes/FileLog.php';
require_once 'Classes/Logger.php';
require_once 'Classes/DatabaseLog.php';

Session::start();

if (!empty(Cookie::check('authKey'))) {
    header('Location: index.php');
    exit();
}

if (Session::checkVal('test')) {
    echo Flash::getMessage('test') . '<br>';
}

$auth = new Auth('email','login','password');
$auth->auth();

$db = new Database();
$db->truncateTable('log')



?>


<h1>Вход</h1>

<form method="post" action="login.php">
    <input type="text" name="login" placeholder="Введите логин.."><br>
    <input type="email" name="email" placeholder="Введите email.."><br>
    <input type="password" name="password" placeholder="Введите пароль.."><br>
    <input type="submit"><br>
</form>
<p>Есле вы <b>не зарегистрированы</b> пожалуйста <a style="color: red" href="register.php">зарегестрируйтесь!</a></p>