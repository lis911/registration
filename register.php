<?php
require_once 'Classes/Flash.php';
require_once 'Classes/Session.php';
require_once 'Classes/Database.php';
require_once 'Classes/Auth.php';
require_once 'Classes/Cookie.php';

Session::start();
if (Session::checkVal('login')){
    header('Location: index.php');
}

$auth = new Auth('email','login');
$auth->registration();

?>
<h1>Регистрация</h1>
<form method="post" action="register.php">
    <input type="text" name="login" placeholder="Введите логин.."><br>
    <input type="email" name="email" placeholder="Введите email.."><br>
    <input type="submit"><br>
</form>

<p>Есле вы <b>зарегистрированы</b> пожалуйста <a style="color: red" href="login.php">войдите!</a></p>

