<?php
namespace Interfaces;

interface Database
{
    /**
     * @param string $tableName
     * @return bool
     */
    public function truncateTable($tableName);

    /**
     * @param string $tableName
     * @param string|array $columns
     * @param array $conditions
     * @param int|null $limit
     * @return array
     */
    public function select($tableName, $columns = '*', $conditions = [], $limit = null);

    /**
     * @param string $tableName
     * @param array $data
     * @return bool
     */
    public function insert($tableName, $data);

    /**
     * @param string $query
     * @return \mysqli_result|bool
     */
    public function execute($query);

    /**
     * @param string $tableName
     * @param array $data
     * @param array $conditions
     * @return bool
     */
    public function update($tableName, $data = [], $conditions = []);
}