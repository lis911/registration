<?php


namespace Interfaces;


interface Logger
{
    /**
     * @param string $login
     * @param string $email
     * @param string $status
     * @return bool
     */
    public static function write($login,$email,$status);

    /**
     * @param array $params
     * @return array
     */
    public static function show($params);
}