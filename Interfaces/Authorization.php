<?php


namespace Interfaces;


interface Authorization
{
    /**
     * Authorization constructor.
     * @param string $keyEmail
     * @param string $keyLogin
     * @param string $keyPassword
     */
    public function __construct($keyEmail, $keyLogin, $keyPassword = '');

    public function registration();

    public function auth();

    /**
     * @param string $authKey
     * @return array
     */
    public static function getUser($authKey);

    public static function logout();
}