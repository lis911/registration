<?php


class Cookie
{
    public static function set($name,$value,$time = null)
    {
        setcookie($name, $value,$time);
    }

    public static function get($name)
    {
        return $_COOKIE[$name] ?? null;
    }

    public static function delete($name)
    {
        setcookie($name, null, time());
    }

    public static function check($name)
    {
        return isset($_COOKIE[$name]);
    }
}