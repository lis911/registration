<?php
require_once 'Classes/Session.php';

class Flash
{
    public static function setMessage($name, $message)
    {
        Session::setVal($name, $message);
    }

    public static function getMessage($name)
    {
        return Session::getVal($name);
    }

}