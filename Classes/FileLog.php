<?php
require_once 'Interfaces/Logger.php';

class FileLog implements \Interfaces\Logger
{
    private static $filename = 'file/log.txt';

    public static function write($login, $email, $status)
    {
        $date = date('Y-m-d H:i:s');
        file_put_contents(self::$filename, "Login: $login, Email: $email, Status: $status, Time: $date \n", FILE_APPEND);
    }

    public static function show($params)
    {
        return file(self::$filename);
    }
}