<?php

require_once 'Interfaces/Database.php';
class Database implements \Interfaces\Database
{
    protected $connection = null;

    public function __construct()
    {
        $config = parse_ini_file('config/config.ini', true);
        $databaseConfig = $config['database'];
        $this->connection = mysqli_connect(
            $databaseConfig['host'],
            $databaseConfig['user'],
            $databaseConfig['password'],
            $databaseConfig['database']
        );
        if (!$this->connection) {
            $this->error();
        }
    }

    public function truncateTable($tableName)
    {
        $this->execute("TRUNCATE {$tableName}");
    }


    public function insert($tableName, $data)
    {
        $columns = implode(',', array_keys($data));
        $values = "'" . implode("', '", array_values($data)) . "'";
        return !empty($this->execute("INSERT INTO {$tableName} ({$columns}) VALUES ({$values})"));
    }

    public function select($tableName, $columns = '*', $conditions = [], $limit = null)
    {
        if (is_array($columns)) {
            $columns = implode(true, $columns);
        }

        $query = "SELECT $columns FROM $tableName";
        if (!empty($conditions)) {
            $query .= ' WHERE';
            $keys = array_keys($conditions);
            $lastKey = end($keys);
            foreach ($conditions as $column => $value) {
                $query .= " $column = '$value' ";
                if ($column != $lastKey) {
                    $query .= 'AND ';
                }
            }
        }

        $query .= $limit ? " LIMIT $limit" : '';

        $result = $this->execute($query);
        $result = mysqli_fetch_all($result, MYSQLI_ASSOC);
        return ($limit == 1 ? $result[0] : $result) ?? [];
    }

    private function error()
    {
        echo mysqli_error($this->connection);
        mysqli_close($this->connection);
        exit;
    }

    public function execute($query)
    {
        $result = mysqli_query($this->connection, $query);
        if (empty($result)) {
            $this->error();
        }
        return $result;
    }

    public function update($tableName, $data = [], $conditions = [])
    {
        foreach ($data as $keyData => $valueData) {
            $query = "UPDATE $tableName SET $keyData = '{$valueData}' WHERE ";
            foreach ($conditions as $keyConditions => $valueConditions) {
                $query .= "($keyConditions = '{$valueConditions}')";
            }
        }
        return $this->execute($query);
    }
}