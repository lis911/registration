<?php


class Session
{
    public static function start()
    {
        session_start();
    }

    public static function setVal($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public static function unsetVal($name)
    {
        unset($_SESSION[$name]);
    }

    public static function getVal($name)
    {
        return $_SESSION[$name];
    }

    public static function checkVal($name)
    {
        return isset($_SESSION[$name]);
    }

    public static function sessionClose()
    {
        session_write_close();
    }

    public static function sessionDestroy()
    {
        session_destroy();
    }
}