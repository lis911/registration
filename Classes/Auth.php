<?php
require_once 'Classes/Flash.php';
require_once 'Classes/DatabaseLog.php';
require_once 'Classes/Session.php';
require_once 'Classes/Database.php';
require_once 'Classes/Cookie.php';
require_once 'Interfaces/Authorization.php';


class Auth implements \Interfaces\Authorization
{
    public $keyEmail;
    public $keyLogin;
    public $keyPassword;

    /**
     * @var Database
     */
    protected $db = null;

    public function __construct($keyEmail, $keyLogin, $keyPassword = '')
    {
        $this->keyEmail = $keyEmail;
        $this->keyLogin = $keyLogin;
        $this->keyPassword = $keyPassword;
        $this->db = new Database();
    }

    public function registration()
    {
        if (!empty($_POST[$this->keyEmail]) && !empty($_POST[$this->keyLogin])) {
            $password = bin2hex(openssl_random_pseudo_bytes(3));
            $this->db->insert('User', [
                'UserLogin' => $_POST[$this->keyLogin],
                'UserEmail' => $_POST[$this->keyEmail],
                'UserPassword' => md5($password),
                'DataTime' => date('Y-m-d H:i:s'),
            ]);

            Flash::setMessage('test', "Вы успешно зарегистрировались!<br>  Ваш пароль для входа: <b>$password</b>");
            header('Location: login.php', true, 301);
        }
    }

    public function auth()
    {
        $login = $_POST[$this->keyLogin] ?? null;
        $email = $_POST[$this->keyEmail] ?? null;
        $password = $_POST[$this->keyPassword] ?? null;
        if ($login && $email && $password) {
            if ($this->comparison($login, $email, $password)) {
                $authKey = md5($login . $email . time());
                $db = new Database();
                $db->update('User', ['auth_key' => $authKey], ['UserLogin' => $login]);
                Cookie::set('authKey', $authKey, time() + 3600);
                Logger::write($login, $email, 'active');
                Flash::setMessage('login', 'Добрый день ');
                header('Location: index.php', true, 301);
                exit();
            } else {
                Logger::write($login, $email, 'inactive');
                echo '<b style="color: red">' . 'Вы ввели неверный логин или пароль!' . '</b>';
            }
        }
    }

    public function comparison($login, $email, $password)
    {
        $result = $this->db->select('User', ['id'], [
            'UserLogin' => $login,
            'UserEmail' => $email,
            'UserPassword' => md5($password),
        ], 1);

        return count($result);
    }

    public static function getUser($authKey)
    {
        $db = new Database();
        return $db->select('User', '*', ['auth_key' => $authKey], 1);
    }

    public static function logout()
    {
        Cookie::delete('authKey');
    }
}