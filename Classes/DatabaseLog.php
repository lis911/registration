<?php
require_once 'Classes/Database.php';
require_once 'Interfaces/Logger.php';

class DatabaseLog implements \Interfaces\Logger
{
    public static function write($login, $email, $status)
    {
        $db = new Database();
        $db->insert('log', [
            'login' => $login,
            'email' => $email,
            'time' => date('Y-m-d H:i:s'),
            'status' => $status,
        ]);
    }

    public static function show($params = [])
    {
        $db = new Database();
        return $db->select('log','*', $params);
    }
}