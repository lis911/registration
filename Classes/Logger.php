<?php
require_once 'DatabaseLog.php';
require_once 'FileLog.php';


class Logger
{
    public static function write($login, $email, $status)
    {
        $config = parse_ini_file('config/config.ini', true);
        $loggerType = $config['logger']['type'] ?? null;

        return call_user_func([$loggerType, 'write'], $login, $email, $status) ?? false;
    }
}